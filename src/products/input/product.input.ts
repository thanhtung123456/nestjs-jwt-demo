
import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';
// import { InputType } from 'type-graphql'



@InputType()
export class ImgProductInput {
    @Field()
    imgfirst?: string;

    @Field()
    imgsecond?: string;

    @Field()
    imgthird?: string;
}

@InputType()
export class RateInput {
    @Field(() => [Number])
    rate?: Number[];
}

@InputType()
export class QuantityInput {
    @Field()
    size?: string;

    @Field()
    QuantityProduct?: number;
}


@InputType()
export class ProductInput {
    // @Field()
    // readonly id: string;

    @Field()
    readonly name?: string;

    @Field()
    readonly brand?: string;

    @Field()
    readonly descProduct?: string;

    @Field(() => Int)
    readonly OldPrice?: number;

    @Field(() => Int)
    readonly NewPrice?: number;

    @Field(type => [String!])
    readonly ColorProduct?: string[];

    // Operation name
    @Field()
    ImgUrlProduct?: ImgProductInput;

    // Operation name
    @Field()
    readonly DetailProduct?: RateInput;

    // Operation name
    @Field(() => [QuantityInput])
    readonly QuantityProductAndSize?: QuantityInput[];

}
