import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ProductsService } from './products.service'
import { ProductDto } from './dto/product.dto'
import { ProductInput, ImgProductInput } from './input/product.input'


@Resolver()
export class ProductsResolver {

    /*
       Constructor(
        private readonly authorsServive ....
     )
    */


    constructor(
        private readonly ProductsService: ProductsService,
    ) { }



    @Query(() => String)
    async hello() {
        return 'hello';
    }

    @Mutation(() => ProductDto)
    async CreateProductGraphql(
        @Args('product') product: ProductInput,
        // @Args('ImgUrlProduct') ImgUrlProduct: ImgProductInput,
    ) {
        return await this.ProductsService.createProductGraphql(product);
    }

    @Mutation(() => ProductDto)
    async UpdateProduct(
        @Args('id') id: string,
        @Args('product') product: ProductInput,
    ) {
        const data = this.ProductsService.findIdAndUpdate(id, product);
        // console.log(data);
        return data;
    }

    @Mutation(() => String)
    async DeleteProduct(
        @Args('id') id: string,
    ) {
        return this.ProductsService.deleteProductGraphql(id);
    }



    @Query(() => [ProductDto!])
    async All() {
        return this.ProductsService.findAllGraphql();
    }

    @Query(() => ProductDto!)
    async FindByIdGraphql(@Args('id') id: string) {

        return this.ProductsService.findByIdGraphql(id);
    }

}


